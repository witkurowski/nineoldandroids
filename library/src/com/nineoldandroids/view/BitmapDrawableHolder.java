package com.nineoldandroids.view;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

/**
 * Created with IntelliJ IDEA.
 * User: dk
 */
public class BitmapDrawableHolder extends DrawableHolder {

    public BitmapDrawableHolder(BitmapDrawable drawable) {
        setDrawable(drawable);
    }

    @Override
    public void setDrawable(Drawable drawable) {
        if (null != drawable && drawable instanceof BitmapDrawable) {
            super.setDrawable(drawable);
            // SetBounds is very important else it's can't be drawn into canvas.
            getDrawable().setBounds(0, 0,
                    drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        }
    }

    public static BitmapDrawableHolder create(Context context, int resId) {
        Drawable drawable = context.getResources().getDrawable(resId);
        if (null != drawable && drawable instanceof BitmapDrawable) {
            return new BitmapDrawableHolder((BitmapDrawable)drawable);
        }
        return null;
    }
}
