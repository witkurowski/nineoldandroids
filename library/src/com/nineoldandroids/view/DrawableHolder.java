package com.nineoldandroids.view;

import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;

/**
 * Created with IntelliJ IDEA.
 * User: dk
 */
public abstract class DrawableHolder {
    private static final int NO_ID = -1;
    protected Drawable mDrawable;
    protected float mX = 0, mY = 0;
    protected int mAlpha = 1;
    protected float mScaleX = 1, mScaleY = 1;
    protected float mRotation = 0;
    protected View mView;
    protected PointF mAnchorPoint = new PointF(0, 0);
    protected boolean mVisibility = true;
    private Object mTag;
    private int mId = NO_ID;


    public void setDrawable(Drawable drawable) {
        mDrawable = drawable;
        updateView();
    }

    public Drawable getDrawable() {
        return mDrawable;
    }

    public void setX(float value) {
        mX = value;
        updateView();
    }

    public float getX() {
        return mX;
    }

    public void setY(float value) {
        mY = value;
        updateView();
    }

    public float getY() {
        return mY;
    }

    public void setPosition(PointF position) {
        if (null != position) {
            setX(position.x);
            setY(position.y);
        }
    }

    public void setAlpha(int alpha) {
        this.mAlpha = alpha;
        if (null != mDrawable) {
            mDrawable.setAlpha(alpha);
            updateView();
        }
    }

    public int getAlpha() {
        return mAlpha;
    }

    public void setScaleX(float scaleX) {
        mScaleX = scaleX;
        updateView();
    }

    public float getScaleX() {
        return mScaleX;
    }

    public void setScaleY(float scaleY) {
        mScaleY = scaleY;
        updateView();
    }

    public float getScaleY() {
        return mScaleY;
    }

    public void setRotation(float rotation) {
        mRotation = rotation;
        updateView();
    }

    public float getRotation() {
        return mRotation;
    }

    public void setScale(float scale) {
        setScaleX(scale);
        setScaleY(scale);
    }

    public void attachView(View view) {
        mView = view;
        if (null != mDrawable) mDrawable.setCallback(view);
    }

    public View getAttachedView() {
        return mView;
    }

    public void setAnchorPointX(float value) {
        mAnchorPoint.x = value;
        updateView();
    }

    public float getAnchorPointX() {
        return mAnchorPoint.x;
    }

    public void setAnchorPointY(float value) {
        mAnchorPoint.y = value;
        updateView();
    }

    public float getAnchorPointY() {
        return mAnchorPoint.y;
    }

    public void setVisibility(boolean isVisibility) {
        mVisibility = isVisibility;
        updateView();
    }

    public boolean getVisibility() {
        return mVisibility;
    }

    public void setAnchorPoint(float x, float y) {
        setAnchorPointX(x);
        setAnchorPointY(y);
    }

    public void setTag(Object tag) {
        mTag = tag;
    }

    public Object getTag() {
        return mTag;
    }

    // 为兼容老版 Android，局部定义硬件加速标志。
    private static final int LAYER_TYPE_HARDWARE = 0x02;
    protected void updateView() {
        if (null != mView) {
            if (null != mDrawable) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    // kane: 从 Android 3.0 开始，默认会通过硬件回事(OpenGL ES)来渲染
                    // View，因此 View.invalidate 方法的参数无效(硬件加速每次直接全屏
                    // 刷新)。所以这种情况下直接调用无参数的 invalidate 方法。
                    if (LAYER_TYPE_HARDWARE == mView.getLayerType()) {
                        mView.invalidate();
                        return;
                    }
                }
                // kane: 如果 View 不是通过硬件加速渲染的（比如在 Android 2.x 下或者主动
                // 关闭了硬件加速特性）,则重绘上一帧造成的脏区域，并更新下一帧将占据的区域。
                // 这种只重绘必要区域的做法将节省一定的渲染时间开销。
                mView.invalidate((int) mDirtyRect.left, (int) mDirtyRect.top, (int) mDirtyRect.right, (int) mDirtyRect.bottom);
                RectF rc = getInvalidateRect();
                mView.invalidate((int)rc.left, (int)rc.top, (int)rc.right, (int)rc.bottom);
            }
        }
    }

    protected RectF mDirtyRect = new RectF();
    protected Matrix mMatrix = new Matrix();
    private RectF mDrawableRect = new RectF();
    // 计算下一帧将要刷新的区域
    RectF getInvalidateRect() {
        final Rect rc = mDrawable.getBounds();
        mDrawableRect.set(rc.left, rc.top, rc.right, rc.bottom);
        float px = rc.width() * mAnchorPoint.x;
        float py = rc.height() * mAnchorPoint.y;
        mMatrix.setScale(mScaleX, mScaleY, px, py);
        mMatrix.postRotate(mRotation, px, py);
        mMatrix.postTranslate(mX - px, mY - py);
        mMatrix.mapRect(mDirtyRect, mDrawableRect);
        return mDirtyRect;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getId() {
        return mId;
    }

    public Rect getBounds() {
        return null != mDrawable ? mDrawable.getBounds() : null;
    }
}
